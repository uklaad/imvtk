**Purpose:** GUI example program written in C++ with VTK enhancement. The manual is written for Windows x64 builds.

**Windows:**

1. Optional [Git](https://git-scm.com/downloads) (let it be all default, just hit next)
2. [MSys2](https://www.msys2.org/) (MinGw)

   * after installation it automatically runs MSYS2 UCRT64 window, so paste this command:

     ```
     pacman -S mingw-w64-ucrt-x86_64-gcc mingw-w64-x86_64-cmake mingw-w64-x86_64-glfw mingw-w64-x86_64-vtk mingw-w64-x86_64-gl2ps mingw-w64-x86_64-freetype --needed base-devel mingw-w64-x86_64-toolchain
     ```

     and let the list be default (hit enter), confirm with Y (Yes)
   * add folder to path: `Control Panel -> System and Security -> System -> Advanced system settings -> Environment Variables`
     see [here](https://code.visualstudio.com/docs/cpp/config-mingw) paragraph 6.
3. Install [VS Code](https://code.visualstudio.com/download)

**Linux:**

1. Run in terminal:
   ```
   sudo apt install code g++ gcc gdb libtbb-dev python3 cmake libvtk9-dev libglfw3-dev dotnet-runtime-6.0
   ```

**VS Code setup**

1. install extensions: `C/C++ Extension Pack, Better Syntax, Better Comments, CMake Language Support, Office Viewer`
2. To use c++20 in code review: `Ctrl+, -> cppstandard -> change the default settings`

**Download, compile and run**

1. Download this [example](https://gitlab.com/uklaad/gui-example)

   * if you installed [Git](https://git-scm.com/downloads), clone git repo to VS Code; if not, just download zip and extract
   * select working dir for the project, open project (trust authors), let it autoconfigure (it should download all necessary stuff), set GCC from menu as a compiler
2. After configuration hit F7 (Build in taskbar) and it should compile with exit code 0
3. Run the program by Shift+F5 (play sign in taskbar) or just open the executable in the program folder

**Program window**

![1670087538305](image/README/1670087538305.png)

**Useful links:**

* Static linking [[1]](https://stackoverflow.com/questions/6404636/libstdc-6-dll-not-found), [[2]](https://stackoverflow.com/questions/13768515/how-to-do-static-linking-of-libwinpthread-1-dll-in-mingw), [[3]](https://stackoverflow.com/questions/18138635/mingw-exe-requires-a-few-gcc-dlls-regardless-of-the-code), [[4]](https://stackoverflow.com/questions/39827753/usr-bin-ld-attempted-static-link-of-dynamic-object-usr-lib64-libm-so)

**Some good additional info:**

* [VSC begin](https://www.youtube.com/watch?v=FcYs8wtzjVE) ; [C++ full course](https://www.youtube.com/watch?v=6y0bp-mnYU0) ; [Obj C++](https://www.youtube.com/watch?v=wN0x9eZLix4) ; [Md Flowcharts](https://mermaid-js.github.io/mermaid/#/flowchart)
