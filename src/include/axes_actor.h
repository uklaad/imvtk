#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkColor.h>
#include <vtkCubeAxesActor.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSuperquadricSource.h>
#include <vtkTextProperty.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkAxesActor.h>
#include <vtkPropAssembly.h>
#include <vtkSmartPointer.h>

static vtkSmartPointer<vtkActor> AxesVTK()
{
  // Define colors for this example
  auto colors = vtkSmartPointer<vtkNamedColors>::New();

  vtkColor3d backgroundColor = colors->GetColor3d("White");
  vtkColor3d actorColor = colors->GetColor3d("Tomato");
  vtkColor3d axis1Color = colors->GetColor3d("Salmon");
  vtkColor3d axis2Color = colors->GetColor3d("PaleGreen");
  vtkColor3d axis3Color = colors->GetColor3d("LightSkyBlue");
auto renderer = vtkSmartPointer<vtkRenderer>::New();

  

  auto cubeAxesActor = vtkSmartPointer<vtkCubeAxesActor>::New();
  cubeAxesActor->SetCamera(renderer->GetActiveCamera());


  cubeAxesActor->SetUseTextActor3D(1);
  cubeAxesActor->SetBounds(0, 1, 0, 1, 0, 2);

  cubeAxesActor->GetTitleTextProperty(0)->SetColor(axis1Color.GetData());
  cubeAxesActor->GetTitleTextProperty(0)->SetFontSize(148);
  cubeAxesActor->GetLabelTextProperty(0)->SetColor(axis1Color.GetData());

  cubeAxesActor->GetTitleTextProperty(1)->SetColor(axis2Color.GetData());
  cubeAxesActor->GetLabelTextProperty(1)->SetColor(axis2Color.GetData());

  cubeAxesActor->GetTitleTextProperty(2)->SetColor(axis3Color.GetData());
  cubeAxesActor->GetLabelTextProperty(2)->SetColor(axis3Color.GetData());

  cubeAxesActor->DrawXGridlinesOn();
  cubeAxesActor->DrawYGridlinesOn();
  cubeAxesActor->DrawZGridlinesOn();
  cubeAxesActor->SetGridLineLocation(cubeAxesActor->VTK_GRID_LINES_FURTHEST);

  cubeAxesActor->XAxisMinorTickVisibilityOff();
  cubeAxesActor->YAxisMinorTickVisibilityOff();
  cubeAxesActor->ZAxisMinorTickVisibilityOff();

  cubeAxesActor->SetFlyModeToStaticEdges();
/* vtkNew<vtkAxesActor> axes;
vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  vtkNew<vtkOrientationMarkerWidget> widget;
  double rgba[4]{0.0, 0.0, 0.0, 0.0};
  colors->GetColor("Carrot", rgba);
  widget->SetOutlineColor(rgba[0], rgba[1], rgba[2]);
  widget->SetOrientationMarker(axes);
  widget->SetInteractor(renderWindowInteractor);
  widget->SetViewport(0.0, 0.0, 0.4, 0.4);
  widget->SetEnabled(1);
  widget->InteractiveOn(); */
  /* renderer->GetActiveCamera()->Azimuth(50);
  renderer->GetActiveCamera()->Elevation(-30);

  renderer->ResetCamera(); */
  return cubeAxesActor;
}