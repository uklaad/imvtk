#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkActor.h>
#include <vtkPoints.h>
#include <math.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkGlyph3D.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLookupTable.h>

#define PI 3.14159265

static vtkSmartPointer<vtkActor> SphereGlyph3d()
{
    srand(time(NULL));

    // create points
    auto points = vtkSmartPointer<vtkPoints>::New();

    // setup scales
    auto scales = vtkSmartPointer<vtkFloatArray>::New();
    scales->SetName("scales");

    // setup color label
    auto col = vtkSmartPointer<vtkFloatArray>::New();
    col->SetName("col");

    // setup lookupTable and add some colors
    auto colors = vtkSmartPointer<vtkLookupTable>::New();
    colors->SetNumberOfTableValues(4);
    colors->SetTableValue(0, 1.0, 0.0, 0.0, 1.0); // red
    colors->SetTableValue(1, 0.0, 1.0, 0.0, 1.0); // green
    colors->SetTableValue(2, 0.0, 0.0, 1.0, 1.0); // blue
    colors->SetTableValue(3, 1.0, 1.0, 0.0, 1.0); // yellow
    // the last double value is for opacity (1->max, 0->min)

    for (int i = 0; i < 1000; i++)
    {
        points->InsertNextPoint(15 * cos(i * PI / 50), 15 * sin(i * PI / 50), 0.1*i); // sphere in circle
        scales->InsertNextValue((rand() % 100) / double(100));                    // random radius between 0 and 0.99
        col->InsertNextValue((rand() % 4));                                       // random color label
    }

    // grid structured to append center, radius and color label
    auto grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid->SetPoints(points);
    grid->GetPointData()->AddArray(scales);
    grid->GetPointData()->SetActiveScalars("scales"); // !!!to set radius first
    grid->GetPointData()->AddArray(col);

    // create anything you want here, we will use a sphere for the demo
    auto sphereSource = vtkSmartPointer<vtkSphereSource>::New();

    // object to group sphere and grid and keep smooth interaction
    auto glyph3D = vtkSmartPointer<vtkGlyph3D>::New();
    glyph3D->SetInputData(grid);
    glyph3D->SetSourceConnection(sphereSource->GetOutputPort());

    // create a mapper and actor
    auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(glyph3D->GetOutputPort());

    mapper->SetScalarModeToUsePointFieldData(); // without, color are displayed regarding radius and not color label
    mapper->SetScalarRange(0, 3);               // to scale color label (without, col should be between 0 and 1)
    mapper->SelectColorArray("col");            // !!!to set color (nevertheless you will have nothing)
    mapper->SetLookupTable(colors);

    auto actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);

    return actor;
}