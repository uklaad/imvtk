#include "include/app_design.h"
// #include "icecream.hpp"

#include <cmath>
#include <vector>
#include <string>
#include <chrono>
#include <thread>
// VTK
#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include "VtkViewer.h"

// File-Specific Includes
#include "include/imgui_vtk_demo.h" // Actor generator for this demo
#include "include/sphere_actor.h"
#include "include/axes_actor.h"
#include "include/sphere_glyph3d.h"

class Parameters
{
public:
    void parameterWindow()
    {
        ImGui::Begin("Set parameters");
        // ImGui::InputScalar("Variable A", ImGuiDataType_Float, &VarA);
        ImGui::SliderFloat("Variable A", &VarA, -10, 10);
        ImGui::Text("Variable A: %f", VarA); // https://en.wikibooks.org/wiki/C%2B%2B_Programming/Code/Standard_C_Library/Functions/printf
        // ImGui::InputScalar("Variable B", ImGuiDataType_Float, &VarB);
        ImGui::SliderFloat("Variable B", &VarB, -1000, 1000);
        ImGui::Text("Variable B: %f", VarB);
        ImGui::Dummy(ImVec2(0.0f, 10.0f));

        ImGui::Text("A+B = %f", VarA + VarB);

        ImGui::Checkbox("Show graph Y = A*X + B", &ShowGraph);
        if (ShowGraph)
        {
            for (int i = 0; i < 100; ++i)
            {
                X[i] = i;
                Y[i] = VarA * X[i] + VarB;
            }
            if (ImPlot::BeginPlot("##XY_Plot"))
            {
                // ImPlot::PlotBars("##",bar_data , 4);
                ImPlot::SetupAxes("X axis", "Y axis", ImPlotAxisFlags_AutoFit, ImPlotAxisFlags_AutoFit);
                ImPlot::PlotLine("##XY_Plot", X, Y, 100);

                ImPlot::EndPlot();
            }
        }

        ImGui::End();
    }

    void setVarA(double varA) { VarA = varA; }
    void setVarB(double varB) { VarB = varB; }
    float getVarA() { return VarA; }
    float getVarB() { return VarB; }

private:
    float VarA = 1.0f;
    float VarB = 0.0f;
    float X[100], Y[100];
    bool ShowGraph = true;
};

struct ExampleAppLog
{
    ImGuiTextBuffer Buf;
    ImGuiTextFilter Filter;
    ImVector<int> LineOffsets; // Index to lines offset. We maintain this with AddLog() calls.
    bool AutoScroll;           // Keep scrolling if already at the bottom.
    bool add = false;

    ExampleAppLog()
    {
        AutoScroll = true;
        Clear();
    }

    void Clear()
    {
        Buf.clear();
        LineOffsets.clear();
        LineOffsets.push_back(0);
    }

    void AddLog(const char *fmt, ...) IM_FMTARGS(2)
    {
        int old_size = Buf.size();
        va_list args;
        va_start(args, fmt);
        Buf.appendfv(fmt, args);
        va_end(args);
        for (int new_size = Buf.size(); old_size < new_size; old_size++)
            if (Buf[old_size] == '\n')
                LineOffsets.push_back(old_size + 1);
    }

    void Draw(const char *title, bool *p_open = NULL)
    {
        if (!ImGui::Begin(title, p_open))
        {
            ImGui::End();
            return;
        }

        // Main menu
        ImGui::Checkbox("Auto-scroll", &AutoScroll);
        ImGui::SameLine();
        add = ImGui::Button("Add");
        ImGui::SameLine();
        bool clear = ImGui::Button("Clear");
        ImGui::SameLine();
        bool copy = ImGui::Button("Copy");
        ImGui::SameLine();
        Filter.Draw("Filter", -100.0f);

        ImGui::Separator();

        if (ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar))
        {
            if (clear)
                Clear();
            if (copy)
                ImGui::LogToClipboard();

            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
            const char *buf = Buf.begin();
            const char *buf_end = Buf.end();
            if (Filter.IsActive())
            {
                // In this example we don't use the clipper when Filter is enabled.
                // This is because we don't have random access to the result of our filter.
                // A real application processing logs with ten of thousands of entries may want to store the result of
                // search/filter.. especially if the filtering function is not trivial (e.g. reg-exp).
                for (int line_no = 0; line_no < LineOffsets.Size; line_no++)
                {
                    const char *line_start = buf + LineOffsets[line_no];
                    const char *line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                    if (Filter.PassFilter(line_start, line_end))
                        ImGui::TextUnformatted(line_start, line_end);
                }
            }
            else
            {
                // The simplest and easy way to display the entire buffer:
                //   ImGui::TextUnformatted(buf_begin, buf_end);
                // And it'll just work. TextUnformatted() has specialization for large blob of text and will fast-forward
                // to skip non-visible lines. Here we instead demonstrate using the clipper to only process lines that are
                // within the visible area.
                // If you have tens of thousands of items and their processing cost is non-negligible, coarse clipping them
                // on your side is recommended. Using ImGuiListClipper requires
                // - A) random access into your data
                // - B) items all being the  same height,
                // both of which we can handle since we have an array pointing to the beginning of each line of text.
                // When using the filter (in the block of code above) we don't have random access into the data to display
                // anymore, which is why we don't use the clipper. Storing or skimming through the search result would make
                // it possible (and would be recommended if you want to search through tens of thousands of entries).
                ImGuiListClipper clipper;
                clipper.Begin(LineOffsets.Size);
                while (clipper.Step())
                {
                    for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
                    {
                        const char *line_start = buf + LineOffsets[line_no];
                        const char *line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                        ImGui::TextUnformatted(line_start, line_end);
                    }
                }
                clipper.End();
            }
            ImGui::PopStyleVar();

            // Keep up at the bottom of the scroll region if we were already at the bottom at the beginning of the frame.
            // Using a scrollbar or mouse-wheel will take away from the bottom edge.
            if (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
                ImGui::SetScrollHereY(1.0f);
        }
        ImGui::EndChild();
        ImGui::End();
    }
};

class ReportWindow
{
public:
    void addReport(Parameters &parameters)
    {
        if (report.add)
        {
            report.AddLog("Sum is %f\n", parameters.getVarA() + parameters.getVarB());
        }
        report.Draw("Report");
    }

private:
    ExampleAppLog report;
};

class VtkWindow
{
public:
    void VtkInit()
    {
        // 0
        auto glyph3d = SphereGlyph3d();
        vtkViewer0.getRenderer()->AddViewProp(glyph3d);

        // 1
        axes = AxesVTK();
        vtkViewer1.getRenderer()->AddViewProp(axes);
        vtkViewer1.getRenderer()->SetBackground(0, 0, 0);
        
        // 2
        auto actor = SetupDemoPipeline();
        vtkViewer2.getRenderer()->AddViewProp(actor);
        vtkViewer2.getRenderer()->SetBackground(0, 0, 0);
    }
    void window0()
    {
        ImGui::SetNextWindowSize(ImVec2(360, 240), ImGuiCond_FirstUseEver);
        ImGui::Begin("Vtk Viewer 0", nullptr, VtkViewer::NoScrollFlags());
        
        vtkViewer0.render(); // default render size = ImGui::GetContentRegionAvail()

        ImGui::End();
    }
    void window1()
    {
        ImGui::SetNextWindowSize(ImVec2(360, 240), ImGuiCond_FirstUseEver);
        ImGui::Begin("Vtk Viewer 1", nullptr, VtkViewer::NoScrollFlags());
        
        ImGui::SliderScalar("X axis", ImGuiDataType_Double, &X, &f64_zero, &f64_one);
        ImGui::SliderScalar("Y axis", ImGuiDataType_Double, &Y, &f64_zero, &f64_one);
        ImGui::SliderScalar("Z axis", ImGuiDataType_Double, &Z, &f64_zero, &f64_one);
        ImGui::SliderScalar("Radius", ImGuiDataType_Double, &R, &f64_zero, &f64_one);
        if(ImGui::Button("Reset camera")){
            vtkViewer1.getRenderer()->SetBackground(1, 0, 0);
            vtkViewer1.getRenderer()->ResetCameraClippingRange(0,0.1,0,0.1,0,0.1);
        }
        vtkViewer1.getRenderer()->RemoveViewProp(sphere);
        sphere = SphereVTK(X, Y, Z, R);

        vtkViewer1.getRenderer()->AddViewProp(sphere);

        vtkViewer1.render(); // default render size = ImGui::GetContentRegionAvail()

        ImGui::End();
    }
    void window2()
    {
        // 5. Show a more complex VtkViewer Instance (Closable, Widgets in Window)
        ImGui::SetNextWindowSize(ImVec2(720, 480), ImGuiCond_FirstUseEver);

        ImGui::Begin("Vtk Viewer 2", nullptr, VtkViewer::NoScrollFlags());

        // Other widgets can be placed in the same window as the VTKViewer
        // However, since the VTKViewer is rendered to size ImGui::GetContentRegionAvail(),
        // it is best to put all widgets first (i.e., render the VTKViewer last).
        // If you want the VTKViewer to be at the top of a window, you can manually calculate
        // and define its size, accounting for the space taken up by other widgets

                if (ImGui::Button("VTK Background: Black"))
        {
            vtkViewer2.getRenderer()->SetBackground(0, 0, 0);
        }
        ImGui::SameLine();
        if (ImGui::Button("VTK Background: Red"))
        {
            vtkViewer2.getRenderer()->SetBackground(1, 0, 0);
        }
        ImGui::SameLine();
        if (ImGui::Button("VTK Background: Green"))
        {
            vtkViewer2.getRenderer()->SetBackground(0, 1, 0);
        }
        ImGui::SameLine();
        if (ImGui::Button("VTK Background: Blue"))
        {
            vtkViewer2.getRenderer()->SetBackground(0, 0, 1);
        }
        static float vtk2BkgAlpha = 0.2f;
        ImGui::SliderFloat("Background Alpha", &vtk2BkgAlpha, 0.0f, 1.0f);
        vtkViewer2.getRenderer()->SetBackgroundAlpha(vtk2BkgAlpha);

        vtkViewer2.render();

        ImGui::End();
    }

private:
    VtkViewer vtkViewer0;
    VtkViewer vtkViewer1;
    VtkViewer vtkViewer2;
    const double f64_zero = 0., f64_one = 1.;
    double X = 1.0;
    double Y = 1.0;
    double Z = 1.0;
    double R = 0.1;
    vtkSmartPointer<vtkActor> sphere;
    vtkSmartPointer<vtkActor> axes;
};

class MyApp : public App<MyApp>
{
public:
    MyApp() = default;
    ~MyApp() = default;

    // ### Your app begins here ###
    virtual void StartUp() final
    {
        vtk.VtkInit();
    }
    virtual void Update() final
    {
        ImGui::ShowDemoWindow();

        parameters.parameterWindow();
        reportWindow.addReport(parameters);
        vtk.window0();
        vtk.window1();
        //vtk.window2();
    }

private:
    Parameters parameters;
    ReportWindow reportWindow;
    VtkWindow vtk;
    // ### Your app ends here ###
};

int main(int, char **)
{
    MyApp app;
    app.Run();

    return 0;
}
